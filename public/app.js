angular.module('Task', ['ngMaterial'])
  .component('taskCreator', {
    controllerAs: 'form',
    templateUrl: 'templates/task/addTask.html',
    controller: class TaskController {
      constructor($http, $rootScope, $timeout) {
        this.$http = $http;
        this.$timeout = $timeout;
        this.$rootScope = $rootScope;
        this.$rootScope.$on('edition', this.onEdit.bind(this));
        this.$rootScope.$on('refresh', this.refresh.bind(this));
        this.refresh();
      }

      refresh() {
        this.task = {
          name: 'Task name',
          priority: '3',
          dueDate: new Date()
        };
      }

      submit({ name, priority, dueDate }) {
        if (!name || !priority || !dueDate) return;

        this.$http({
          url: '/api/task/create',
          method: 'POST',
          params: { name, priority, dueDate: dueDate.toISOString().split('T')[0] }
        }).then(msg => this.$rootScope.$emit('changed'))
          .catch(msg => console.log(msg))
      }

      onEdit(event, task) {
        this.task = task;
      }
    }
  })
  .component('taskList', {
    controllerAs: 'form',
    templateUrl: 'templates/task/taskList.html',
    controller: class TaskController {
      constructor($http, $timeout, $rootScope) {
        this.$http = $http;
        this.$timeout = $timeout;
        this.$rootScope = $rootScope;
        this.tasks = [];
        this.paint();
        this.$rootScope.$on('changed', this.paint.bind(this));
      }

      paint() {
        this.$http.get('/api/task')
          .then(this.refresh.bind(this))
          .catch(msg => console.log(msg));
      }

      refresh({ data }) {
        this.$timeout(() => this.tasks = data.reverse());
      }

      destroy(id) {
        this.$rootScope.$emit('refresh');
        this.$http.get(`/api/task/destroy/${id}`)
          .then(this.paint.bind(this))
          .catch(msg => console.log(msg));
      }

      edit(task) {
        this.$rootScope.$emit('edition', angular.copy(task));
      }

    }
  });