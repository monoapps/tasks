const express = require('express');
const bp = require('body-parser');
const validator = require('ajv');
const taskSchema = require('./schemas/task');

const app = express();
const router = express.Router();
const port = process.env.PORT || 8080;
let tasks = [{ "dueDate": "2018-06-22", "name": "Task name", "priority": "3", "id": 2, "createdAt": "2018-06-22T02:51:21.473Z", "updatedAt": "2018-06-22T02:51:21.473Z" }];

app.use(bp.urlencoded({ extended: true }));
app.use(bp.json());

router.post('/create', (req, res) => {
  const ajv = new validator();
  const valid = ajv.validate(taskSchema, req.query);
  if (!valid) return res.send(500, { status: 500, errors: ajv.errors });

  const task = {
    ...req.query,
    id: tasks.length + 1,
    createdAt: new Date().toISOString(),
    updatedAt: new Date().toISOString(),
  };
  tasks.push(task);
  res.json(task);
});

router.get('/', (req, res) =>
  res.json(tasks));

router.get('/destroy/:id', (req, res) => {
  const task = tasks.find(({ id }) => `${id}` === req.params.id);
  tasks = tasks.filter(({ id }) => `${id}` !== req.params.id);
  res.json(task);
});

router.post('/update/:id', (req, res) => {
  const task = tasks.find(({ id }) => `${id}` === req.params.id);
  //TODO: Define api
});

app.use(express.static(__dirname + '/public'));
app.use('/api/task/', router);
app.set('views', './views')
app.listen(port, () => console.log(`Server Up and Running http://127.0.0.1:${port}`));
